# Facts

## [Facts Assignment Apk](https://drive.google.com/file/d/10bKTVMEeOtw6hSlwC2GuvLWqRDCKnf_O/view?usp=sharing)

## [Try App on Web Browser](https://appetize.io/app/bjgav09navh11r1yuuy3j1gfng)

[![API](https://img.shields.io/badge/API-14%2B-brightgreen.svg?style=flat)](https://android-arsenal.com/api?level=14)

## Overview of Assignment
* Will show the feed from [link](https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json)
* App Follows MVVM Architecture Design Pattern
* Dependency Injection implemented using Dagger
* Room used for Local Database
* Images are loaded Lazily using Glide
* Random Splash Animation Before going to DashBoard using xml files.
* Retrofit is Used for the Rest API integration.
* Swipe to Down to Refresh and Manual Refresh.
* Landscape and Portrait Mode Support.
* Ripple Effect on Item Touch
* Android UI Test Cases and Unit Test Cases are covered.
* Bitbucket and Git are used for VCS.
* 100 % Code in Kotlin

## Libraries Used

[Retrofit](https://square.github.io/retrofit/)  [Glide](https://github.com/bumptech/glide) 
[Room](https://developer.android.com/topic/libraries/architecture/room) [Dagger](https://dagger.dev/dev-guide/android.html)

### Assignment By
# [Harsh Sharma](http://harsh159357.github.io/)

Technology Analyst Infosys

[StackOverFlow](http://bit.ly/stackharsh)
[LinkedIn](http://bit.ly/lnkdharsh)
[Facebook](http://bit.ly/harshfb)
