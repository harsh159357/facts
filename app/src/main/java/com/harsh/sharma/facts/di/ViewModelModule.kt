package com.harsh.sharma.facts.di

import androidx.lifecycle.ViewModel
import com.harsh.sharma.facts.ui.factslist.FactsListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(FactsListViewModel::class)
    abstract fun factsListViewModel(viewModel: FactsListViewModel): ViewModel


}