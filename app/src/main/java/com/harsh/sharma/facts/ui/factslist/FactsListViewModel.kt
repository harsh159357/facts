package com.harsh.sharma.facts.ui.factslist

import androidx.lifecycle.MutableLiveData
import com.harsh.sharma.facts.data.FactsRepository
import com.harsh.sharma.facts.data.local.model.Row
import com.harsh.sharma.facts.data.remote.Result
import com.harsh.sharma.facts.di.CoroutineScopeIO
import com.harsh.sharma.facts.ui.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import javax.inject.Inject

class FactsListViewModel @Inject constructor(
    private val factsRepository: FactsRepository,
    @CoroutineScopeIO private val coroutineScope: CoroutineScope
) : BaseViewModel() {

    var mutableListLiveDataResult: MutableLiveData<Result<List<Row>>> = MutableLiveData()
    var connectivityAvailable = false

    /**
     * Get Facts list from data repository
     */
    fun getFactsList() {
        factsRepository.observeFacts(coroutineScope) {
            mutableListLiveDataResult.postValue(it)
        }
    }

    /**
     * Cancel all coroutines when the ViewModel is cleared.
     */
    override fun onCleared() {
        super.onCleared()
        coroutineScope.cancel()
    }

}