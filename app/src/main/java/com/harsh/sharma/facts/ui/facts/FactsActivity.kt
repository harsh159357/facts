package com.harsh.sharma.facts.ui.facts

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.harsh.sharma.facts.R
import com.harsh.sharma.facts.data.remote.Result
import com.harsh.sharma.facts.di.ViewModelFactory
import com.harsh.sharma.facts.di.injectViewModel
import com.harsh.sharma.facts.ui.base.BaseActivity
import com.harsh.sharma.facts.ui.factslist.FactsListViewModel
import com.harsh.sharma.facts.ui.factslist.adapter.NewListAdapter
import com.harsh.sharma.facts.util.ConnectivityUtil
import com.harsh.sharma.facts.util.NetworkStateReceiver
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.activity_facts.*
import javax.inject.Inject


class FactsActivity : BaseActivity(), HasAndroidInjector,
    SwipeRefreshLayout.OnRefreshListener, NetworkStateReceiver.NetworkStateReceiverListener {

    private var networkCheck: Boolean = false
    private var netWorkStateReceiver: NetworkStateReceiver? = null

    companion object {
        @JvmStatic
        fun createIntent(context: Context): Intent = Intent(context, FactsActivity::class.java)
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector

    override val layoutId: Int
        get() = R.layout.activity_facts

    @Inject
    lateinit var viewModel: ViewModelFactory

    private lateinit var newListViewModel: FactsListViewModel

    private var adapter: NewListAdapter? = null

    override fun initializeViewModel() {
        newListViewModel = injectViewModel(viewModel)
        newListViewModel.connectivityAvailable = ConnectivityUtil.isConnected(this)
    }

    override fun onResume() {
        super.onResume()
        initViews()
    }

    private fun initViews() {
        rv_facts.setHasFixedSize(true)
        initSwipeToRefresh()
        when (adapter) {
            null -> {
                adapter = NewListAdapter()
                rv_facts.adapter = adapter
                adapter?.let {
                    subscribeUi(it)
                }
                newListViewModel.getFactsList()
            }
            else -> {
                rv_facts.adapter = adapter
            }
        }
        netWorkStateReceiver = NetworkStateReceiver(this)
        netWorkStateReceiver!!.addListener(this)
        registerReceiver(
            netWorkStateReceiver,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(netWorkStateReceiver)
    }

    private fun initSwipeToRefresh() {
        swipeRefreshLayout.setOnRefreshListener(this)
        swipeRefreshLayout.setColorSchemeResources(
            R.color.colorPrimary,
            android.R.color.holo_green_dark,
            android.R.color.holo_orange_dark
        )
    }

    /**
     * Observer all list to update UI on data change. If MutableLiveData already has data
     * set, it will be delivered to the observer.
     * When data changes views will receive the last available data from the server and
     * local database.
     */
    private fun subscribeUi(adapter: NewListAdapter) {
        newListViewModel.mutableListLiveDataResult.observe(this, { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    rv_facts.visibility = View.VISIBLE
                    result.title?.let {
                        supportActionBar?.setDisplayShowTitleEnabled(true)
                        if (it.isNotEmpty()) {
                            supportActionBar?.title = it
                        } else {
                            supportActionBar?.title = getString(R.string.facts)
                        }
                    }
                    result.data?.let {
                        adapter.submitList(it)
                        if (it.isNotEmpty()) {
                            empty_container.visibility = View.GONE
                            rv_facts.visibility = View.VISIBLE
                        } else {
                            empty_container.visibility = View.VISIBLE
                            rv_facts.visibility = View.GONE
                        }
                    }
                    swipeRefreshLayout.isRefreshing = false
                }
                Result.Status.LOADING -> {
                    rv_facts.visibility = View.GONE
                    swipeRefreshLayout.isRefreshing = true
                }
                Result.Status.ERROR -> {
                    swipeRefreshLayout.isRefreshing = false
                    result.message?.let {
                        empty_container.visibility = View.VISIBLE
                        rv_facts.visibility = View.GONE
                        Snackbar.make(rv_facts, it, Snackbar.LENGTH_LONG).show()
                    }
                }
            }
        })
    }

    override fun onRefresh() {
        newListViewModel.getFactsList()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_facts, menu)
        return true

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_refresh -> onRefresh()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        AlertDialog.Builder(this)
            .setMessage(R.string.exit)
            .setPositiveButton(
                R.string.yes
            ) { dialog, which -> finish() }
            .setNegativeButton(
                R.string.no
            ) { dialog, which -> dialog.cancel() }
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }

    override fun onNetworkAvailable() {
        if (empty_container.visibility == View.VISIBLE) {
            onRefresh()
        }
    }

    override fun onNetworkUnavailable() {
    }

}
