package com.harsh.sharma.facts.ui.base

import androidx.lifecycle.ViewModel

abstract class BaseViewModel: ViewModel()