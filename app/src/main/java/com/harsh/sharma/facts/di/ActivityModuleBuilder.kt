package com.harsh.sharma.facts.di

import com.harsh.sharma.facts.ui.facts.FactsActivity
import com.harsh.sharma.facts.ui.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ActivityModuleBuilder {

    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): FactsActivity
}