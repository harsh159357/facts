package com.harsh.sharma.facts.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "rows")
data class Row(
    @PrimaryKey(autoGenerate = true)
    var id: Long,

    var title: String,

    var description: String,

    var imageHref: String
) {
    constructor() : this(
        0, "", "", ""
    )
}