package com.harsh.sharma.facts.ui.factslist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.harsh.sharma.facts.R
import com.harsh.sharma.facts.data.local.model.Row

class NewListAdapter : ListAdapter<Row, FactsViewHolder>(DiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FactsViewHolder {
        return FactsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_facts, parent, false)
        )
    }

    override fun onBindViewHolder(holder: FactsViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

private class DiffCallback : DiffUtil.ItemCallback<Row>() {
    override fun areItemsTheSame(oldItem: Row, newItem: Row): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Row, newItem: Row): Boolean {
        return oldItem == newItem
    }
}