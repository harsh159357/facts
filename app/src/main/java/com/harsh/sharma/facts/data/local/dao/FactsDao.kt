package com.harsh.sharma.facts.data.local.dao

import androidx.room.*
import com.harsh.sharma.facts.data.local.model.Row

@Dao
interface FactsDao {
    /**
     * Get all Facts
     */
    @Transaction
    @Query("SELECT * FROM rows ORDER BY id")
    fun getAllFacts(): List<Row>

    /**
     * Get Rows by rowId
     */
    @Transaction
    @Query("SELECT * FROM rows WHERE id =:rowId")
    fun getRowsById(rowId: Long): Row

    /**
     *Get Facts Size
     */
    @Transaction
    @Query("SELECT Count(*) FROM rows")
    fun getFactsSize(): Int

    /**
     * Insert all Facts
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllFacts(rowList: List<Row>)

    /**
     * Delete all Facts
     */
    @Query("DELETE FROM rows")
    fun deleteAllFacts()
}