package com.harsh.sharma.facts.ui.factslist.adapter

import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.harsh.sharma.facts.R
import com.harsh.sharma.facts.data.local.model.Row
import com.harsh.sharma.facts.util.bindImageFromUrl
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_facts.*

class FactsViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
    LayoutContainer {

    /**
     * Called when onBindViewHolder is triggered in RecyclerView adapter
     * The new bind will be used to set the values to display items
     */
    fun bind(row: Row) {

        val context = containerView.context

        when {
            row.title.isEmpty() -> {
                txtFactTitle.text = context.getString(R.string.no_title)
            }
            else -> {
                txtFactTitle.text = row.title
            }
        }
        when {
            row.description.isEmpty() -> {
                txtFactDesc.text = context.getString(R.string.no_description)
            }
            else -> {
                txtFactDesc.text = row.description
            }
        }

        bindImageFromUrl(imgFact, row.imageHref)

        root_view.setOnClickListener {
            Toast.makeText(context, txtFactTitle.text, Toast.LENGTH_SHORT).show()
        }
    }
}