package com.harsh.sharma.facts.data.local

import com.harsh.sharma.facts.data.local.dao.FactsDao
import com.harsh.sharma.facts.data.local.model.Row
import com.harsh.sharma.facts.data.remote.dto.Facts
import javax.inject.Inject

class LocalRepository @Inject
constructor(private val dao: FactsDao) {

    /**
     * Insert all facts into database
     */
    fun saveFacts(factsModel: List<Facts>) {
        clearDatabase()
        val rowList: ArrayList<Row> = arrayListOf()
        for (factsItem in factsModel) {
            val factsModelItem = Row()
            factsItem.title?.let {
                factsModelItem.title = it
            }
            factsItem.description?.let {
                factsModelItem.description = it
            }
            factsItem.imageHref?.let {
                factsModelItem.imageHref = it
            }
            rowList.add(factsModelItem)
        }
        dao.insertAllFacts(rowList)
    }

    /**
     * Get All Facts
     */
    fun getFactsList() = dao.getAllFacts()

    /**
     * Get Facts List Size
     */
    fun getFactsListSize() = dao.getFactsSize()

    /**
     * Clear all Facts
     */
    private fun clearDatabase() {
        dao.deleteAllFacts()
    }

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: LocalRepository? = null

        fun getInstance(dao: FactsDao) =
            instance ?: synchronized(this) {
                instance
                    ?: LocalRepository(dao).also { instance = it }
            }
    }
}