package com.harsh.sharma.facts.data.remote.dto

import com.google.gson.annotations.SerializedName

/**
 * Fields for Individual Fact coming from Json
 */
data class Facts(
    @SerializedName("description")
    var description: String? = null,
    @SerializedName("imageHref")
    var imageHref: String? = null,
    @SerializedName("title")
    var title: String? = null
)