package com.harsh.sharma.facts.ui.splash

import android.os.Bundle
import android.os.Handler
import android.view.MotionEvent
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.harsh.sharma.facts.R
import com.harsh.sharma.facts.ui.base.BaseActivity
import com.harsh.sharma.facts.ui.facts.FactsActivity
import com.harsh.sharma.facts.util.Constants
import com.harsh.sharma.facts.util.getRandomSplashAnimation
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : BaseActivity() {

    override val layoutId: Int
        get() = R.layout.activity_splash

    private var mAnimation: Animation? = null

    private var splashHandler: Handler? = null
    private val splashRunnable = Runnable { navigateToHomeScreen() }

    override fun initializeViewModel() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        doAnimation()
    }

    private fun doAnimation() {
        mAnimation = AnimationUtils
            .loadAnimation(this, getRandomSplashAnimation())
        mAnimation?.run { setDuration(Constants.SPLASH_DELAY.toLong()) }
        splash_image.startAnimation(mAnimation)
        app_name.startAnimation(mAnimation)
        splashHandler = Handler()
        splashHandler?.run { postDelayed(splashRunnable, Constants.SPLASH_DELAY.toLong()) }
    }

    override fun onTouchEvent(evt: MotionEvent): Boolean {
        if (evt.action == MotionEvent.ACTION_DOWN) {
            splashHandler!!.removeCallbacks(splashRunnable)
            navigateToHomeScreen()
        }
        return true
    }

    private fun navigateToHomeScreen() {
        startActivity(FactsActivity.createIntent(this))
        finish()
    }
}
