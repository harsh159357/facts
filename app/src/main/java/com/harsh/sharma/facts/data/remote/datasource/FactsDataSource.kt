package com.harsh.sharma.facts.data.remote.datasource

import com.harsh.sharma.facts.data.remote.BaseDataSource
import com.harsh.sharma.facts.data.remote.FactsService
import javax.inject.Inject

class FactsDataSource @Inject constructor(private val factsService: FactsService) :
    BaseDataSource() {
    suspend fun fetchFacts() = getResult { factsService.fetchFacts() }
}
