package com.harsh.sharma.facts.util

import com.harsh.sharma.facts.R

class Constants {

    companion object {
        const val SPLASH_DELAY = 2000

        const val BASE_URL = "https://dl.dropboxusercontent.com/"

        const val FACTS_JSON = "s/2iodh4vg0eortkl/facts.json"

        const val FACTS_END_POINT: String = BASE_URL + FACTS_JSON


        //Some sample splash animations
        val splashAnimation = intArrayOf(
            R.anim.fade_in,
            R.anim.zoom_in,
            R.anim.slide_down,
            R.anim.bounce_down,
            R.anim.rotate_clockwise,
            R.anim.rotate_anti_clockwise
        )

    }
}