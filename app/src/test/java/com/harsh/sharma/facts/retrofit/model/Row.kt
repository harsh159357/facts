package com.harsh.sharma.facts.retrofit.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Row : Parcelable {
    @SerializedName("title")
    var title: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("imageHref")
    var imageHref: String? = null

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(title)
        dest.writeString(description)
        dest.writeString(imageHref)
    }

    constructor() {}
    protected constructor(`in`: Parcel) {
        title = `in`.readString()
        description = `in`.readString()
        imageHref = `in`.readString()
    }

    companion object {
        val CREATOR: Parcelable.Creator<Row> =
            object : Parcelable.Creator<Row> {
                override fun createFromParcel(source: Parcel): Row? {
                    return Row(source)
                }

                override fun newArray(size: Int): Array<Row?> {
                    return arrayOfNulls(size)
                }
            }
    }
}