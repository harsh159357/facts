package com.harsh.sharma.facts.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.harsh.sharma.facts.ConnectionUtil
import mockit.Mocked
import mockit.NonStrictExpectations
import org.junit.Assert
import org.junit.Test

class AppUtilTest {
    @Mocked
    var mMockContext: Context? = null

    @Mocked
    var networkInfo: NetworkInfo? = null

    @Mocked
    var connectivityManager: ConnectivityManager? = null

    @Test
    fun isConnectionAvailable(): Unit {
        object : NonStrictExpectations() {
            init {
                mMockContext!!.getSystemService(Context.CONNECTIVITY_SERVICE)
                returns(connectivityManager)
                connectivityManager!!.activeNetworkInfo
                returns(networkInfo)
                networkInfo!!.state
                returns(NetworkInfo.State.CONNECTED)
            }
        }
        mMockContext?.let { ConnectionUtil.isConnected(it) }?.let { Assert.assertTrue(it) }
    }

    @Test
    fun isConnectionNotAvailable(): Unit {
        object : NonStrictExpectations() {
            init {
                mMockContext!!.getSystemService(Context.CONNECTIVITY_SERVICE)
                returns(connectivityManager)
                connectivityManager!!.activeNetworkInfo
                returns(networkInfo)
                networkInfo!!.state
                returns(NetworkInfo.State.DISCONNECTED)
            }
        }
        mMockContext?.let { ConnectionUtil.isConnected(it) }?.let { Assert.assertFalse(it) }
    }
}