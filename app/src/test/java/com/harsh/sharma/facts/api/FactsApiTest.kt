package com.harsh.sharma.facts.api

import com.harsh.sharma.facts.ConnectionUtil
import com.harsh.sharma.facts.TestUtil
import com.harsh.sharma.facts.retrofit.ApiInterface
import com.harsh.sharma.facts.retrofit.model.Facts
import com.harsh.sharma.facts.util.Constants
import org.junit.*
import org.junit.runners.MethodSorters

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class FactsApiTest {
    private var apiInterface: ApiInterface? = null

    @Before
    fun beforeTest() {
        // Initializing Api Interface
        apiInterface = TestUtil.apiInterface
    }

    @Throws(InterruptedException::class)
    @Test
    fun factsSuccessfulTest(): Unit {
        val apiResponse =
            apiInterface!!.getFacts(Constants.FACTS_JSON)?.let { ConnectionUtil.execute<Any>(it) }
        val facts =
            apiResponse?.response as Facts
        Assert.assertNotNull(facts)
        Assert.assertTrue("Title is there", facts.title!!.isNotEmpty())
        Assert.assertTrue("Rows are there", facts.rows.size > 0)
    }

    @Throws(InterruptedException::class)
    @Test
    fun factsUnSuccessfulTest(): Unit {
        val apiResponse =
            apiInterface!!.getFacts(Constants.BASE_URL)?.let { ConnectionUtil.execute<Any>(it) }
        val facts =
            apiResponse?.response
        Assert.assertNull("Facts are not loaded", facts)
    }

    @After
    fun afterTest() {
        apiInterface = null
    }
}