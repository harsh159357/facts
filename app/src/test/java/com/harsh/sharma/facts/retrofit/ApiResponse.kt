package com.harsh.sharma.facts.retrofit

import okhttp3.Headers

//Used for mapping response fetched from server and other useful things
class ApiResponse<T> {
    var responseCode: Int
    var response: T
    var headerParams: Headers? = null

    constructor(responseCode: Int, response: T) : super() {
        this.responseCode = responseCode
        this.response = response
    }

    constructor(responseCode: Int, response: T, headers: Headers?) : super() {
        this.responseCode = responseCode
        this.response = response
        headerParams = headers
    }

}