package com.harsh.sharma.facts;

import com.harsh.sharma.facts.api.FactsApiTest;
import com.harsh.sharma.facts.model.FactsTest;
import com.harsh.sharma.facts.model.RowTest;
import com.harsh.sharma.facts.utils.AppUtilTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({

        FactsApiTest.class,
        FactsTest.class,
        RowTest.class,
        AppUtilTest.class

})

public class TelstraAssignmentTestSuite {
}  	