package com.harsh.sharma.facts.retrofit

import com.harsh.sharma.facts.retrofit.model.Facts
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

// Test Api Interface Used for Api Integration using Retrofit
interface ApiInterface {
    @GET
    fun getFacts(@Url url: String?): Call<Facts?>?
}