package com.harsh.sharma.facts

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import com.harsh.sharma.facts.retrofit.ApiResponse
import retrofit2.Call
import retrofit2.Response
import java.io.IOException

//Used for making connection with APIs
object ConnectionUtil {
    private val TAG = ConnectionUtil::class.java.name
    fun <T> execute(call: Call<*>): ApiResponse<*>? {
        try {
            val response: Response<out Any> = call.execute()
            return ApiResponse<Any?>(response.code(), response.body(), response.headers())
        } catch (e: IOException) {
            Log.d(TAG, "Error in execute api request")
        } catch (ex: Exception) {
            Log.d(TAG, "Error in execute api" + ex.message)
        }
        return null
    }

    fun isConnected(context: Context): Boolean {
        val cm =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = cm.activeNetworkInfo ?: return false
        val network = info.state
        return network == NetworkInfo.State.CONNECTED || network == NetworkInfo.State.CONNECTING
    }
}