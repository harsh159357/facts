package com.harsh.sharma.facts.model

import android.content.Context
import android.content.res.AssetManager
import com.google.gson.Gson
import com.harsh.sharma.facts.TestUtil
import com.harsh.sharma.facts.retrofit.model.Facts
import mockit.Mocked
import mockit.NonStrictExpectations
import mockit.integration.junit4.JMockit
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.InputStream

@RunWith(JMockit::class)
class FactsTest {
    private var facts: Facts? = null
    private var json: String? = null

    @Mocked
    var mMockContext: Context? = null

    @Mocked
    var assetManager: AssetManager? = null
    private var productListInputStream: InputStream? = null

    @Before
    @Throws(Exception::class)
    fun setUp() {
        readFactsJson(TestUtil.FACTS_BEST_CASE_JSON)
    }

    @After
    fun tearDown() {
        json = null
    }

    @Test
    fun testTitleNotNull() {
        Assert.assertNotNull(facts!!.title)
    }

    @Test
    fun testTitleCheck() {
        Assert.assertEquals("About Canada", facts!!.title)
        Assert.assertNotEquals("AboutCanada", facts!!.title)
    }

    @Test
    fun testRowsIsEmpty() {
        Assert.assertFalse(facts!!.rows.isEmpty())
    }

    @Test
    fun testRowsCount() {
        Assert.assertEquals(9, facts!!.rows.size.toLong())
        Assert.assertNotEquals(15, facts!!.rows.size.toLong())
    }

    @Throws(Exception::class)
    private fun readFactsJson(resourceFileName: String) {
        productListInputStream = javaClass.classLoader.getResourceAsStream(resourceFileName)
        object : NonStrictExpectations() {
            init {
                mMockContext!!.assets
                returns(assetManager)
                assetManager!!.open(resourceFileName)
                returns(productListInputStream)
            }
        }
        json = mMockContext?.let { TestUtil.loadJSONFromAssets(resourceFileName, it) }
        facts = Gson().fromJson(
            json,
            Facts::class.java
        )
    }
}