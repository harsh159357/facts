package com.harsh.sharma.facts

import android.content.Context
import com.harsh.sharma.facts.retrofit.ApiInterface
import com.harsh.sharma.facts.util.Constants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.nio.charset.Charset
import java.util.concurrent.TimeUnit

object TestUtil {
    const val CHARSET_NAME = "UTF-8"
    const val FACTS_BEST_CASE_JSON = "facts_best_case.json"
    const val WORST_CASE_JSON = "facts_worst_case.json"
    fun loadJSONFromAssets(
        fileName: String?,
        context: Context
    ): String? {
        var json: String? = null
        json = try {
            val `is` = context.assets.open(fileName!!)
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            String(buffer, Charset.forName(CHARSET_NAME))
        } catch (ex: Exception) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    val apiInterface: ApiInterface
        get() {
            val builder = OkHttpClient.Builder()
            builder.connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
            val client = builder.build()
            val retrofit = Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
}