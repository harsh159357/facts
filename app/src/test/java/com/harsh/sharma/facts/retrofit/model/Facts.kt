package com.harsh.sharma.facts.retrofit.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.util.*

class Facts : Parcelable {
    @SerializedName("title")
    var title: String? = null

    @SerializedName("rows")
    var rows =
        ArrayList<Row?>()

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(title)
        dest.writeList(rows)
    }

    constructor() {}
    protected constructor(`in`: Parcel) {
        title = `in`.readString()
        rows = ArrayList()
        `in`.readList(
            rows,
            Row::class.java.classLoader
        )
    }

    companion object {
        val CREATOR: Parcelable.Creator<Facts> =
            object : Parcelable.Creator<Facts> {
                override fun createFromParcel(source: Parcel): Facts? {
                    return Facts(source)
                }

                override fun newArray(size: Int): Array<Facts?> {
                    return arrayOfNulls(size)
                }
            }
    }
}