package com.harsh.sharma.facts.model

import android.content.Context
import android.content.res.AssetManager
import com.google.gson.Gson
import com.harsh.sharma.facts.TestUtil
import com.harsh.sharma.facts.retrofit.model.Facts
import mockit.Mocked
import mockit.NonStrictExpectations
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.InputStream

class RowTest {
    private var facts: Facts? = null
    private var json: String? = null

    @Mocked
    var mMockContext: Context? = null

    @Mocked
    var assetManager: AssetManager? = null
    private var productListInputStream: InputStream? = null

    @Before
    @Throws(Exception::class)
    fun setUp() {
        readFactsJson(TestUtil.WORST_CASE_JSON)
    }

    @After
    fun tearDown() {
        json = null
    }

    @Test
    fun testTitle() {
        Assert.assertEquals("Geography", facts!!.rows[3]?.title)
    }

    @Test
    fun testTitleNotNull() {
        Assert.assertNotNull(facts!!.rows[0]?.title)
    }

    @Test
    fun description(): Unit {
        Assert.assertEquals(
            "Nous parlons tous les langues importants.",
            facts!!.rows[4]?.description
        )
    }

    @Test
    fun descriptionNotNull(): Unit {
        Assert.assertNotNull(facts!!.rows[1]?.description)
    }

    @Test
    fun testImageHref() {
        Assert.assertEquals(
            "http://images.findicons.com/files/icons/662/world_flag/128/flag_of_canada.png",
            facts!!.rows[0]?.imageHref
        )
    }

    @Test
    fun imageHrefNull(): Unit {
        Assert.assertNull(facts!!.rows[3]?.imageHref)
    }

    @Throws(Exception::class)
    private fun readFactsJson(resourceFileName: String) {
        productListInputStream = javaClass.classLoader.getResourceAsStream(resourceFileName)
        object : NonStrictExpectations() {
            init {
                mMockContext!!.assets
                returns(assetManager)
                assetManager!!.open(resourceFileName)
                returns(productListInputStream)
            }
        }
        json = mMockContext?.let { TestUtil.loadJSONFromAssets(resourceFileName, it) }
        facts = Gson().fromJson(
            json,
            Facts::class.java
        )
    }
}