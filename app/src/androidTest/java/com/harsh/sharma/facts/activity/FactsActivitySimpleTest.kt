package com.harsh.sharma.facts.activity

import android.app.Activity
import android.content.Intent
import android.os.SystemClock
import androidx.test.InstrumentationRegistry
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import androidx.test.runner.lifecycle.Stage
import com.harsh.sharma.facts.R
import com.harsh.sharma.facts.ui.facts.FactsActivity
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class FactsActivitySimpleTest {
    @Rule @JvmField
    var rule =
        ActivityTestRule(FactsActivity::class.java, true, false)
    private var activity: Activity? = null

    @Before
    @Throws(Exception::class)
    public fun setUp() {
    }

    @Test
    fun testDashBoardActivity() {
        statActivity()
        Espresso.onView(ViewMatchers.withId(R.id.empty_container)).check(
            ViewAssertions.matches(
                ViewMatchers.withChild(
                    ViewMatchers.withText(
                        activityInstance!!.resources
                            .getString(R.string.no_facts)
                    )
                )
            )
        )
        Espresso.onView(ViewMatchers.withId(R.id.menu_refresh))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.rv_facts))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun testActivityIsDashBoardActivity() {
        statActivity()
        activity = activityInstance
        Assert.assertTrue(activity is FactsActivity)
    }

    @Test
    fun testActivityNotDestroyed() {
        statActivity()
        activity = activityInstance
        Assert.assertFalse(activity!!.isDestroyed)
    }

    private fun statActivity() {
        val intent = Intent()
        activity = rule.launchActivity(intent)
        SystemClock.sleep(2000)
    }

    companion object {
        val activityInstance: Activity?
            get() {
                val currentActivity = arrayOf<Activity?>(null)
                try {
                    InstrumentationRegistry.getInstrumentation()
                        .runOnMainSync {
                            val resumedActivities: Collection<*> =
                                ActivityLifecycleMonitorRegistry.getInstance()
                                    .getActivitiesInStage(Stage.RESUMED)
                            if (resumedActivities.iterator().hasNext()) {
                                currentActivity[0] =
                                    resumedActivities.iterator().next() as Activity?
                            }
                        }
                } catch (e: Exception) {
                }
                return currentActivity[0]
            }
    }
}