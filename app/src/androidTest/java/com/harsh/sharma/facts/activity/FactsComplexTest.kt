package com.harsh.sharma.facts.activity

import android.app.Activity
import android.content.Intent
import android.os.SystemClock
import android.view.View
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.rule.ActivityTestRule
import com.harsh.sharma.facts.R
import com.harsh.sharma.facts.ui.facts.FactsActivity
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class FactsComplexTest {
    @Rule
    @JvmField
    var activityTestRule = ActivityTestRule(
        FactsActivity::class.java, true, false
    )
    private var activity: Activity? = null

    @Before
    @Throws(Exception::class)
    fun setUp() {
    }

    @Test
    fun testFactsClickInActivity() {
        statActivity()
        SystemClock.sleep(1000)
        Espresso.onView(withIndex(ViewMatchers.withId(R.id.imgFact), 2))
            .perform(ViewActions.click())
    }

    @Test
    fun testFactsListIsSwipeAbleUpAndDown() {
        statActivity()
        SystemClock.sleep(1000)
        Espresso.onView(ViewMatchers.withId(R.id.rv_facts))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(ViewMatchers.withId(R.id.rv_facts))
            .perform(ViewActions.swipeDown())

        SystemClock.sleep(1000)

        Espresso.onView(ViewMatchers.withId(R.id.rv_facts))
            .perform(ViewActions.swipeUp())
    }

    @Test
    fun testRecyclerViewItems() {
        statActivity()
        SystemClock.sleep(1000)
        Espresso.onView(ViewMatchers.withId(R.id.rv_facts))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun testPerformRefresh() {
        statActivity()
        SystemClock.sleep(1000)
        Espresso.onView(ViewMatchers.withId(R.id.menu_refresh)).perform(ViewActions.click())
    }


    private fun statActivity() {
        val intent = Intent()
        activity = activityTestRule.launchActivity(intent)
        SystemClock.sleep(1000)
    }

    companion object {
        fun withIndex(
            matcher: Matcher<View?>,
            index: Int
        ): TypeSafeMatcher<View?> {
            return object : TypeSafeMatcher<View?>() {
                var currentIndex = 0
                override fun describeTo(description: Description) {
                    description.appendText("with index: ")
                    description.appendValue(index)
                    matcher.describeTo(description)
                }

                public override fun matchesSafely(view: View?): Boolean {
                    return matcher.matches(view) && currentIndex++ == index
                }
            }
        }
    }
}